import pycuda.autoinit
import pycuda.driver as drv
import numpy as np
from pycuda.compiler import SourceModule

import modul1

print modul1.funkcija()
print modul1.varijabla

mod = SourceModule(open("zrna.cu").read())
vector_sum = mod.get_function("vector_sum")

a = np.ones(500, dtype=np.float32)
b = np.ones(500, dtype=np.float32)

result_gpu = np.zeros_like(a)

vector_sum(drv.Out(result_gpu), drv.In(a), drv.In(b),
           block=(250,1,1), grid=(2,1))

result_cpu = a + b

print "CPU rezultat\n", result_cpu
print "GPU rezultat\n", result_gpu
print "CPU i GPU daju isti rezultat?\n", (result_cpu == result_gpu).all()
